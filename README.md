# Подготовка MVP инфраструктурной платформы для приложения-примера

Репозиторий с кодом приложения - `https://gitlab.com/d.kochetov1/microservice-demo`

Репозиторий с кодом инфраструктуры - `https://gitlab.com/d.kochetov1/otus-project`

### Доступ к сервисам кластера
Grafana - http://grafana.35.225.80.119.nip.io - (admin:prom-operator)

Prometheus - http://prometheus.35.225.80.119.nip.io

Alertmanager - http://alertmanager.35.225.80.119.nip.io

Приложение - http://35.225.80.119.nip.io

Kibana - https://kibana.35.225.80.119.nip.io

### Этапы PipeLine microservice-demo:
1) build

Выполняется запрос для запуска pipeline из репозитория otus-project

### Этапы PipeLine otus-project:
1) build

Выполняется сборка docker образа для приложения, Dockerfile расположен в каталоге `docker-drupal`
Собранный образ пушится в Contaner Registry https://gitlab.com/d.kochetov1/microservice-demo/container_registry
Также в артефактах сохраняется config.json для docker для дольнейшего добавления в секреты kubernetes.

2) create_cluster

С помощью terraform выполняется создание кластера kubernetes в gke. 
Состояние terraform хранится удаленно в https://app.terraform.io/app/otus-project/workspaces/final_project/runs, для этого задан параметр `backend`
По завершения настройки кластера выполняется создания файла kubeconfig и сохранение его в артефактах.

3) settings_cluster

На данном этапе выполняется установка gcloud, kubectl, helm
Конфигурация переменных окружения для доступа к файлам конфигурации kubectl
После выполнения настройки kubectl происходит деплой в кластер nginx-ingress, cert-manager, prometheus-operator.
Также выполняется настройка доменов в переменных для helm в соответсвии с полученным ip для loadbalancer.
После настройки кластера выполняется деплой приложения с помощью helm chart-ов.


### Описание приложения на CMS Drupal
Для разворачивания приложения требуется три контейнера:
- php
- mysql
- memcached

Для контейнера php был подготовлен DockerFile, упаковывающий приложения в docker image.
Чарты для деплоя приложения расположены в каталоге - charts/microservice/
Для mysql и memcached используются публичные чарты из репозитория https://kubernetes-charts.storage.googleapis.com, настройки выполнены в файле переменных charts/microservice/values.yaml.monitoring/configmap_memcached.yaml

### Мониторинг
Для мониторинга используется prometheus-operator, устанавливаемый вместе с kube-state-metrics, prometheus-node-exporter, grafana, alertmanager, prometheus.
Деплой мониторинга выолняется с помощью чарта - `https://github.com/helm/charts/tree/master/stable/prometheus-operator`
Настройка деплоя выполняются в файле `monitoring/values.yaml` - Указывается настройка ingress для сервисов, Отключается использование serviceMonitorSelector для Prometheus для применения всех ресурсов ServiceMonitor.
Для сервисов drupal(php) и memcached в настройках указывается создавать конетйнер в поде для экспорта метрик, а также создается ресурс кластера ServiceMonitor для мониторинга метрик служб apache, memcached. На странице http://prometheus.35.225.80.119.nip.io/service-discovery в namespace default два сервиса для созданных Servicemonitor.
monitoring/configmap_memcached.yaml
Для создания dashboard-ов для служб apache, memcached подготовлены configmap monitoring/configmap_apache.yaml и monitoring/configmap_memcached.yaml, которые sidecar контейнером добавляется в Grafana:
http://grafana.35.225.80.119.nip.io/d/k1uVqSBWk/memcached?orgId=1
http://grafana.35.225.80.119.nip.io/d/dSs1uSfZz/apache?orgId=1&refresh=1m


### Логирование
Для централизованного сбора логов использован стэк программ
Elasticsearch + Kibana + Fluent-bit
Для этого был изменен виртуальный хост apache так, чтобы логи писались в stdout и stderr.
Затем fluent-bit собирает логи контейнеров в кластере и направляет их в elasticsearch.
Далее с помощью kibana подключаемся к elastic и просматриваем содержимое индекса(предварительно нужно выполнить создание логов обращением к сайту http://35.225.80.119.nip.io)
Для просмотра индекса нужно:
- перейти на https://kibana.35.225.80.119.nip.io
- в левом меню выбрать discover
- создать индекс по шаблону `kubernetes_cluster-*`
- Filter Field указать @timestamp
- Вернуть в меню Discover
- в строке поиска ввести `kubernetes.pod_name:microservice`
- в результате получим логи apache о выполненных запросах

